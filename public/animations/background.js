/// Animated background Canvas by Francisco Martins
let canvas;
let handler;
let particles = [];

const mouse = {
    x: window.innerHeight / 2,
    y: window.innerWidth / 2
};

/// Drawing methods
window.addEventListener('mousemove', event => {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

function Random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function RandomColor() {
    return "rgba(" + Random(0, 255) + "," + Random(0, 255) + "," + Random(0, 255) + ",0." + Random(1, 3) + ")";
}

function Particle(posX, posY, radius, color) {
    const velocity = 0.002;
    const startAngle = 0;

    this.x = posX;
    this.y = posY;
    this.radius = radius;
    this.color = color;
    this.radians = Math.random() * Math.PI * 2;
    this.ranges = {
        x: Random(Random(window.innerHeight * 2, 0), window.innerHeight),
        y: Random(Random(window.innerWidth * 2, 0), window.innerWidth)
    };

    this.animate = () => {
        this.radians += velocity;
        this.x = posX + Math.cos(this.radians) * this.ranges.x;
        this.y = posY + Math.sin(this.radians) * this.ranges.y;
        this.draw();
    }

    this.draw = () => {
        handler.beginPath();
        handler.arc(this.x, this.y, this.radius, startAngle, Math.PI * 2, false);
        handler.fillStyle = this.color;
        handler.fill();
        handler.closePath();

        return this;
    }

}

/// Animation methods

function Animate() {
    handler.clearRect(0, 0, canvas.width, canvas.height);

    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;

    particles.forEach(particle => {
        particle.animate();
    });

    requestAnimationFrame(Animate);
    console.log("Background drawing!");
}

/// Styling
canvas = document.getElementById("App-animated-background");
canvas.height = window.innerHeight;
canvas.width = window.innerWidth;
canvas.style.backgroundImage = "radial-gradient(#38195d, rgb(26, 7, 48), rgb(10 4 19))";
canvas.style.position = "absolute";
canvas.style.zIndex = "-1";
canvas.style.top = "0";

/// Rendering
handler = canvas.getContext("2d");

for (let i = 0; i < (window.innerHeight * window.innerWidth) / 1000; i++)
    particles.push(new Particle(canvas.width / 2, canvas.height / 2, 10, RandomColor()));

Animate();
