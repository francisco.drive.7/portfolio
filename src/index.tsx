import React from 'react';
import ReactDOM from 'react-dom';

import './styles/App.css';

import Homepage from './containers/homepage/Homepage';
import Navigation from './components/Navigation';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
    <React.StrictMode>
        <Navigation/>
        <Homepage/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);
