import React from 'react';

function Homepage() {
    return (
        <section className="App">
            <div className="App-header">
                <h1>Francisco Martins portfolio</h1>
                <ul className="list-group App-ref-socials">
                    <li className="list-group-item">
                        <a href="https://github.com/0xfcmartins" target="_blank" rel="noreferrer">
                            <i className="fab fa-github"/>
                            <span className="App-ref-socials-text">GitHub</span>
                        </a>
                    </li>
                    <li className="list-group-item">
                        <a href="https://twitter.com/0xfcmartins">
                            <i className="fab fa-twitter"/>
                            <span className="App-ref-socials-text">Twitter</span>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
    );
}

export default Homepage;
